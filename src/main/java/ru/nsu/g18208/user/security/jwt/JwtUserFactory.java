package ru.nsu.g18208.user.security.jwt;

import ru.nsu.g18208.model.User;

public final class JwtUserFactory {

    public JwtUserFactory() {

    }

    public static JwtUser create(User user) {
        return new JwtUser(user.getUserId(), user.getUsername(), user.getPassword(), user.getCategories());
    }

}
